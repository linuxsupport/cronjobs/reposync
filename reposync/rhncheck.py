#!/usr/bin/env python

import sys
import json
import base64
import glob
import io
import os
import tempfile
import smtplib
import shutil
import subprocess
import zipfile
from time import sleep
from datetime import datetime, timezone
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import requests

requests.packages.urllib3.disable_warnings(
    requests.packages.urllib3.exceptions.InsecureRequestWarning
)


def send_email(email_to, subject, body, email_from='linux.support@cern.ch', kill=False):
    server = smtplib.SMTP('cernmx.cern.ch')
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = email_from
    msg['To'] = email_to
    msg.add_header('reply-to', 'noreply.Linux.Support@cern.ch')
    formatted_body = MIMEText(
        f"Dear admins,\n\n{body}\n\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)",
        _subtype="plain",
    )
    msg.attach(formatted_body)

    try:
        server.sendmail(email_from, email_to, msg.as_string())
        sleep(2)
    except:
        print(f"failed to send email to {email_to}, continuing...")
    if kill:
        sys.exit(1)


def get_uuids(input_data, uuid_names):
    uuids = []
    for name in uuid_names:
        for entry in input_data:
            if entry.get('name') == name and entry.get('lastCheckin') is not None:
                last_checkin = datetime.strptime(entry.get('lastCheckin'), "%Y-%m-%dT%H:%M:%S.%fZ")
                current_date = datetime.now(timezone.utc).replace(tzinfo=None)
                difference = current_date - last_checkin
                difference_in_days = difference.total_seconds() / (24 * 60 * 60)
                if difference_in_days > 14:
                    # requests.delete is not working with https://access.redhat.com/management/api/rhsm#/system/removeSystem
                    # This shouldn't be a problem as RedHat now removes stale entries after a threshold anyway
                    print("UUID is stale. We should delete it, but the API isn't letting us")
                    print("Creating a new one in its place")
                    cmd = f"subscription-manager register --activationkey {activation_key} --force --name {name} --org {org}"
                    process = subprocess.Popen(cmd, stderr=subprocess.PIPE, shell=True, stdout=subprocess.PIPE)
                    out, err = process.communicate()
                    print(f"Output: {out}")
                    if err:
                        print(f"Error creating a new uuid: {err}")
                        sys.exit(1)
                    uuids.append(str(out).split('ID: ')[1].split('\\n')[0])
                    break
                else:
                    uuids.append(entry.get('uuid'))
                    break
    return uuids

def get_token(ot):
    url = "https://sso.redhat.com/auth/realms/redhat-external/protocol/openid-connect/token"
    data = {
        "grant_type": "refresh_token",
        "client_id": "rhsm-api",
        "refresh_token": ot,
    }
    result = requests.post(url, data=data)
    if result.status_code != 200:
        print("unable to auth, exiting")
    try:
        token = json.loads(result.content)["access_token"]
    except json.decoder.JSONDecodeError:
        print("unable to auth, exiting")
    return token


def call_https_rhsm(url, token=None):
    retries = 0
    base64string = base64.encodebytes(f"{login}:{password}".encode())
    headers = {'Authorization': f"Basic {base64string.decode().strip()}"}
    while retries <= 3:
        if token is not None:
            try:
                result = requests.get(url, headers={"Authorization": f"Bearer {token}"})
            except Exception as e:
                pass
        else:
            try:
                result = requests.get(url, headers=headers, verify=False)
            except Exception as e:
                pass
        if result.status_code == requests.codes.ok:
            # Basically, here we will have a zip file of the entitlements
            if 'content-disposition' in result.headers:
                return io.BytesIO(result.content)
            return json.loads(result.content)
        print('Failed ({}) to get {}'.format(result.status_code, url))
        retries += 1
        sleep(retries**3)
    return False


def get_entitlements(directory, linuxsoft_uuid):
    url = f"https://subscription.rhsm.redhat.com/subscription/consumers/{linuxsoft_uuid}/certificates"
    certificates = call_https_rhsm(url)
    if not certificates:
        send_email(
            admin_email,
            "[ERROR] reposync: Unable to download certificates",
            f"I failed to download certificates from the {url} url. Something seems broken",
            kill=True,
        )
    try:
        z = zipfile.ZipFile(certificates)
    except zipfile.BadZipFile:
        send_email(
            admin_email,
            "[ERROR] reposync: Bad zip file has been downloaded",
            f"The zip file downloaded from rhsm is corrupt. Something seems broken",
            kill=True,
        )
    z.extractall(directory)
    try:
        cz = zipfile.ZipFile(f"{directory}/consumer_export.zip")
    except zipfile.BadZipFile:
        send_email(
            admin_email,
            "[ERROR] reposync: Unable to extract entitlement zip file from rhsm",
            f"Unable to extract zip file from {url}. Something seems broken",
            kill=True,
        )
    cz.extractall(directory)
    for cert in os.listdir(f"{directory}/export/content_access_certificates"):
        print(f"Downloading/Extracting rhn cert: {cert}")
        shutil.move(f"{directory}/export/content_access_certificates/{cert}", directory)


admin_email = os.getenv('RHNCHECK_ADMIN_EMAIL')
login = os.getenv('RHNCHECK_RHN_USER')
password = os.getenv('RHNCHECK_RHN_PASSWORD')
rhn_systems = os.getenv('RHNCHECK_SYSTEMS')
offline_token = os.getenv('RHSM_OFFLINE_TOKEN')
activation_key = os.getenv('RHN_ACTIVATION_KEY')
org = os.getenv('RHN_ORG')
if (
    admin_email is None
    or login is None
    or password is None
    or rhn_systems is None
    or offline_token is None
    or activation_key is None
    or org is None
):
    send_email(
        admin_email,
        "[ERROR] reposync: Variabes not set",
        "Critical variables are not set (admin_email, login, password, rhn_systems, offline_token, activation_key, or org). Unable to continue.",
        kill=True,
    )

linuxsoft_systems = rhn_systems.split(',')

tmpdir = tempfile.mkdtemp(dir='/tmp')
existingcerts = set()
latestcerts = set()

for existingcert in glob.glob('/certs/*pem'):
    if (
        'rhn_entitlements.pem' not in existingcert
        and 'redhat-uep.pem' not in existingcert
    ):
        existingcerts.add(os.path.basename(existingcert))

token = get_token(offline_token)

url = "https://api.access.redhat.com/management/v1/systems"
result = call_https_rhsm(url, token)
if not result:
    send_email(
        admin_email,
        "[ERROR] reposync: Unable to list systems",
        f"I failed to list the systems from {url}. Something seems broken",
        kill=True,
    )
body_list = result['body']
body_list = [x for x in body_list if 'lastCheckin' in x.keys()]
body_list = sorted(body_list, key=lambda d: d['lastCheckin'], reverse=True)
linuxsoft_uuids = get_uuids(body_list, linuxsoft_systems)
if len(linuxsoft_uuids) != len(linuxsoft_systems):
    send_email(
        admin_email,
        "[ERROR] reposync: Unable to map a uuid",
        f"Unable to map a uuid for a system defined via the RHNCHECK_SYSTEM variable ({rhn_systems}). Something seems broken",
        kill=True,
    )

for uuid in linuxsoft_uuids:
    get_entitlements(tmpdir, uuid)

for cert in glob.glob(f"{tmpdir}/*pem"):
    latestcerts.add(os.path.basename(cert))

if latestcerts.difference(existingcerts):
    print('New certificates detected from RHN, copying them in place')
    new_cert_filenames = set()
    for cert in latestcerts.difference(existingcerts):
        new_cert_filenames.add(os.path.basename(cert))
    for cert in glob.glob('/certs/*pem'):
        if 'redhat-uep.pem' not in cert:
            os.remove(cert)
    for f in latestcerts:
        try:
            shutil.copy(f"{tmpdir}/{f}", f"/certs/{f}")
        # Ignore problems if /certs is not mounted (eg: during testing)
        except FileNotFoundError:
            pass

    email_certs = "\n".join(new_cert_filenames)
    send_email(
        admin_email,
        "[INFO] reposync: cdn.redhat.com certificate changes detected",
        f"Changes were detected with the RedHat entitlement certificates.\n\nThe new certificate(s):\n\n{email_certs}\n\nare now deployed\n\nNo action is required by real humans :)",
    )

else:
    print('No cert changes today :)')
