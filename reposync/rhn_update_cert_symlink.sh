#!/bin/bash
URL=$(grep baseurl /etc/yum.repos.d/sync.repo | cut -d= -f2)
REPOID=$(head -1 /etc/yum.repos.d/sync.repo | cut -d[ -f2 | cut -d] -f1)
echo $REPOID > /etc/yum/vars/certificate
CHECK=$(curl -L -k -E /certs/$REPOID $URL --write-out %{http_code} --silent --output /dev/null)
if [ "$CHECK" == "200" ]; then
  echo "Certificate symlink for $REPOID is good, nothing to do"
else
  echo "Current certificate for $REPOID doesn't auth, let's fix that"
  pushd /certs >/dev/null
  for CERT in `ls *.pem`
  do
    STATUS_CODE=$(curl -L -k -E /certs/$CERT $URL --write-out %{http_code} --silent --output /dev/null)
    if [ "$STATUS_CODE" == "200" ]; then
      echo "$CERT is valid for $REPOID"
      ln -sf $CERT $REPOID
      echo $REPOID > /etc/yum/vars/certificate
      break
    fi
  done
  popd >/dev/null
fi
