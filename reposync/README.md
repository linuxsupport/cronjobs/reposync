# reposync

## Debugging

1. Check out this repo somewhere where you can build docker images.
1. Create a `config.json` file with the following content:
  ```{"REPOPATH": "download.opensuse.org/repositories/network:/bro/CentOS_7/", "REPOID": "bro-centos-7", "REPOFILE": "name=The Bro Network Security Monitor (CentOS_7)\ntype=rpm-md\nbaseurl=http://download.opensuse.org/repositories/network:/bro/CentOS_7/\nenabled=1\ngpgcheck=1\ngpgkey=file:///etc/pki/rpm-gpg/misc/RPM-GPG-KEY-bro"}```
1. Build the docker image: `docker build . -t reposync`
1. Run your image with the following volume mounts:
  `docker run -v ~/reposync/reposync/config.json:/local/config.json -v /tmp/alloc:/alloc -v /tmp/repo:/repo reposyn`
