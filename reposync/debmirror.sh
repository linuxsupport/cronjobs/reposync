#!/bin/bash

# Verify baseurl
grep -q 'deburl' /etc/yum.repos.d/sync.repo
if [ $? -eq 0 ]; then
  URL=$(sed '/deburl=/!d; s/.*=//; s#file://##g' /etc/yum.repos.d/sync.repo)
else
  echo "Please add the deburl to mirror"
  exit
fi

URL_FORMATTED="${URL#https://}"
URL_FORMATTED="${URL_FORMATTED#http://}"
HOSTNAME=$(echo "$URL_FORMATTED" | awk -F'/' '{print $1}')
FULL_PATH="${URL_FORMATTED/$HOSTNAME}"

# Verify distribution
grep -q 'distribution' /etc/yum.repos.d/sync.repo
if [ $? -eq 0 ]; then
  DISTRIBUTION=$(sed '/distribution=/!d; s/.*=//; s#file://##g' /etc/yum.repos.d/sync.repo)
else
  echo "Please add the distribution to .repo file"
  exit
fi

# Verify section
grep -q 'distribution' /etc/yum.repos.d/sync.repo
if [ $? -eq 0 ]; then
  SECTION=$(sed '/section=/!d; s/.*=//; s#file://##g' /etc/yum.repos.d/sync.repo)
else
  echo "Please add the section to .repo file"
  exit
fi

debmirror -a amd64 -s $SECTION -h $HOSTNAME -d $DISTRIBUTION -r $FULL_PATH --progress --method=http  --rsync-extra=none --no-check-gpg /repo/mirror/${HOSTNAME}/${FULL_PATH}
