#!/bin/bash
#
# Zoom only populates 1 rpm named 'zoom_x86_64.rpm', which makes
# life very difficult to a) install and b) provide updates
# This script essentially copies the statically named zoom rpm
# to a NVR named file

TMPDIR=`mktemp -d`
cd $TMPDIR
curl --silent -L -O https://cern.zoom.us/client/latest/zoom_x86_64.rpm
VR=`rpm --queryformat "%{VERSION}-%{RELEASE}" -qp zoom_x86_64.rpm 2>/dev/null`
FULL_PACKAGE_NAME="zoom-$VR.x86_64.rpm"
PACKAGES=`find $REPOPATH -type f -name "$FULL_PACKAGE_NAME" | wc -l`
if [ $PACKAGES -eq 0 ]; then
  echo "rpm $FULL_PACKAGE_NAME does not exist, adding now"
  cp $TMPDIR/zoom_x86_64.rpm $REPOPATH/$FULL_PACKAGE_NAME
  cd $REPOPATH
  createrepo -d .
else
  echo "$FULL_PACKAGE_NAME already exists, nothing to see here"
fi
