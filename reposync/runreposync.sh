#!/bin/bash

OUTPUT='/alloc/output.json'

log () {
  mapfile IN
  cat << EOF | tr -d '\n'; echo
{ "repoid": "${REPOID}",
  ${IN[@]}
}
EOF
}

error () {
  mapfile IN
  cat << EOF | log
    "message_type": "error",
    ${IN[@]}
EOF
}

echo "Inputs: REPOID=\"$REPOID\" REPOFILE=\"$REPOFILE\" REPOPATH=\"$REPOPATH\" CHECKSUM=\"$CHECKSUM\" RUN_REPOSYNC=\"$RUN_REPOSYNC\" RUN_CUSTOMSCRIPT=\"$RUN_CUSTOMSCRIPT\" CUSTOMSCRIPT=\"$CUSTOMSCRIPT\" SKIP_PACKAGE_COUNT=\"$SKIP_PACKAGE_COUNT\""

REPOPATH="/repo/${REPOPATH}"
if [[ $SKIP_PACKAGE_COUNT -eq 0 ]]; then
  # Record what's already there first
  /usr/bin/find ${REPOPATH} -type f -name "*.rpm" ! -size 0 -printf '%f %s\n' > /tmp/prelist
  PRECOUNT=$(wc -l /tmp/prelist | cut -d' ' -f1)
  PRESIZE=$(awk '{print $2}' /tmp/prelist | paste -sd+ | bc)
else
  PRECOUNT=0
fi

echo "[${REPOID}]" > /etc/yum.repos.d/sync.repo
echo $REPOFILE | base64 -d >> /etc/yum.repos.d/sync.repo

if [[ $RUN_CUSTOMSCRIPT -eq 1 ]]; then
  eval $CUSTOMSCRIPT
fi

# Don't try to download asynchronously
# There's a crappy bug in that async code somewhere, this avoids it
printf "\nasync=0\n" >> /etc/yum.repos.d/sync.repo


# Create REPOPATH even if the repo is empty
if [ ! -d $REPOPATH ]; then
  /bin/mkdir -p ${REPOPATH}
  PRECOUNT=-1
fi

# Check if there are packages in the upstream repo, and stop if there aren't.
UPSTREAM_COUNT=$(repoquery -a --refresh --disablerepo='*' --enablerepo="${REPOID}" | wc -l)
if [[ $PRECOUNT -ne 0 && $UPSTREAM_COUNT -eq 0 ]]; then
  RET=1
  cat << EOF | error | tee $OUTPUT
  "exit_code": $RET,
  "error": "No packages in upstream repo, aborting"
EOF
  exit $RET
fi

# Let's make absolutely sure there's only one copy of the job running
LOCKFILE="$REPOPATH/.reposynclock"
if [[ -f "$LOCKFILE" ]]; then
  # The lockfile exists, let's check if it's fresh
  AGE=$((`date +%s` - `stat -c '%Y' $LOCKFILE`))
  if [[ $AGE -gt 86400 ]]; then
    echo "$LOCKFILE is over 24 hours old. It's probably stale, so get rid of it."
    rm -f "$LOCKFILE"
  fi
fi

# Grab the lock, exit if you fail
if !(set -o noclobber; echo "`hostname` ($$)" > "$LOCKFILE") 2> /dev/null; then
  echo "Lock exists: $LOCKFILE owned by $(cat $LOCKFILE)"
  exit
fi

# Delete the lock if we die
trap 'rm -f "$LOCKFILE"; exit $?' INT TERM EXIT

# Verify gpgcheck
grep -q 'gpgcheck=1' /etc/yum.repos.d/sync.repo
if [ $? -eq 0 ]; then
  GPGCHECK='--gpgcheck'
fi

if [[ $RUN_REPOSYNC -eq 1 ]]; then

  # Run reposync
  /usr/bin/reposync \
    --assumeyes \
    --delete \
    --remote-time \
    --downloadcomps \
    --download-metadata \
    --norepopath \
    ${GPGCHECK} \
    --download-path ${REPOPATH} \
    --repoid ${REPOID} 2> >(tee /local/stderr)
  RET=$?

  if grep -q "Error: GPG signature check failed." /local/stderr; then
    # Hardcode the exit code to 2 so we can detect this error in the job
    RET=2
    cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "reposync gpg error",
    "output": "$(sed 's/"/\\"/g' /local/stderr)"
EOF
  elif [[ $RET -ne 0 ]]; then
    cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "reposync error",
    "output": "$(sed 's/"/\\"/g' /local/stderr)"
EOF
    exit $RET
  fi

  # Remove 0 byte RPMs
  /usr/bin/find ${REPOPATH} -type f -size 0b -name "*.rpm" -exec rm -v {} \;
fi

# Sync GPG key from repo config file. Put it on repo root path
if grep -q 'gpgkey=' /etc/yum.repos.d/sync.repo; then
  GPGKEYPATH=$(sed '/gpgkey=/!d; s/.*=//; s#file://##g' /etc/yum.repos.d/sync.repo)
  REPOROOTPATH=$(echo $REPOPATH | awk -F "/" '{print "/"$2"/"$3"/"$4"/"}')
  cp -u -v --preserve=timestamps $GPGKEYPATH $REPOROOTPATH
fi

if [[ $SKIP_PACKAGE_COUNT -eq 0 ]]; then
  # Now let's look at the new stuff
  /usr/bin/find ${REPOPATH} -type f -name "*.rpm" ! -size 0 -printf '%f %s\n' > /tmp/postlist
  POSTCOUNT=$(wc -l /tmp/postlist | cut -d' ' -f1)

  # cdn.redhat.com delivers us some rpms with junk attached at the end ... ?
  # this may not be an issue anymore, but let's track it just in case
  POSTSIZE=$(awk '{print $2}' /tmp/postlist | paste -sd+ | bc)

  if [[ ($PRECOUNT -eq $POSTCOUNT) && ($PRESIZE -ne $POSTSIZE) ]]; then
    cat << EOF | error | tee $OUTPUT
      "error": "WARNING: no rpm count change but size change (pre: $PRESIZE, post: $POSTSIZE)"
EOF
  fi

  if [[ ($PRECOUNT -ne $POSTCOUNT) || ($PRESIZE -ne $POSTSIZE) ]]; then
    CHANGELIST=$(/usr/bin/diff --changed-group-format='%>' --unchanged-group-format='' <(awk '{print $1}' /tmp/prelist) <(awk '{print $1}' /tmp/postlist))

    # Keep the current xml.gz file and remove the older one
    NEWUPDXMLFILE=$(sed '/updateinfo.xml/!d; s/.*repodata\/\(.*\)".*/\1/' ${REPOPATH}/repodata/repomd.xml)
    [ -n "$NEWUPDXMLFILE" ] && /usr/bin/find ${REPOPATH} -name '*updateinfo.xml*' -not -name $NEWUPDXMLFILE -printf "removed '%f'\n" -delete

    # Keep the current yaml.gz file and remove the older one
    NEWMODULEFILE=$(sed '/yaml.gz/!d; s/.*repodata\/\(.*\)".*/\1/' ${REPOPATH}/repodata/repomd.xml)
    [ -n "$NEWMODULEFILE" ] && /usr/bin/find ${REPOPATH} -name '*module*.yaml*' -not -path ${REPOPATH}/repodata/$NEWMODULEFILE -printf "removed '%f'\n" -delete
  fi
else
  POSTCOUNT=0
fi

for rpm in ${CHANGELIST}; do
  path=$(/usr/bin/find ${REPOPATH} -type f -name "${rpm}")
  info=$(/usr/bin/rpm --queryformat "%{NAME} %{VERSION}-%{RELEASE} %{ARCH}" -qp ${path} 2>/dev/null)
  name=$(echo $info | cut -d' ' -f1)
  version=$(echo $info | cut -d' ' -f2)
  arch=$(echo $info | cut -d' ' -f3)

  cat << EOF | log
    "message_type": "change",
    "rpm_name": "${name}",
    "rpm_version": "${version}",
    "rpm_arch": "${arch}",
    "rpm_filename": "${rpm}"
EOF
done

CHANGECOUNT=$((POSTCOUNT-PRECOUNT))

cat << EOF | log
  "message_type": "result",
  "exit_code": ${RET},
  "pre_count": ${PRECOUNT},
  "post_count": ${POSTCOUNT},
  "change_count": ${CHANGECOUNT}
EOF

# Turn the new-line separated list of changes into a comma-separated list of quoted strings
LIST=$(sed 's/^/"/;s/$/"/;s/^""$//' <(echo "${CHANGELIST}") | /usr/bin/tr '\n' ',' | sed 's/,$//')

cat << EOF | log > $OUTPUT
  "message_type": "result",
  "exit_code": ${RET},
  "pre_count": ${PRECOUNT},
  "post_count": ${POSTCOUNT},
  "changes": [${LIST}]
EOF

# Clean up after yourself, and release your trap
rm -f "$LOCKFILE"
trap - INT TERM EXIT
