job "${PREFIX}_reposync_${REPOID}" {
  datacenters = ["*"]

  type = "batch"

  spread {
    attribute = "${node.unique.name}"
  }

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  reschedule {
    attempts       = 6
    interval       = "1h"
    delay          = "5m"
    delay_function = "fibonacci"
    max_delay      = "1h"
    unlimited      = false
  }

  priority = 30

  task "${PREFIX}_reposync_${REPOID}" {
    driver = "docker"

    restart {
      attempts = 1
      delay    = "1m"
      interval = "5m"
      mode     = "fail"
    }

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/reposync/reposync:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_reposync"
        }
      }
      volumes = [
        "${MOUNTPOINT}:/repo",
        "${RHNCERTDIR}:/certs",
      ]
    }

    env {
      REPOID = "$REPOID"
      REPOFILE = "$REPOFILE"
      REPOPATH = "$REPOPATH"
      CHECKSUM = "$CHECKSUM"
      CUSTOMSCRIPT = "$CUSTOMSCRIPT"
      RUN_REPOSYNC = "$RUN_REPOSYNC"
      RUN_CUSTOMSCRIPT = "$RUN_CUSTOMSCRIPT"
      SKIP_PACKAGE_COUNT = "$SKIP_PACKAGE_COUNT"
      RHNCHECK_ADMIN_EMAIL = "$RHNCHECK_ADMIN_EMAIL"
      RHNCHECK_RHN_USER = "$RHNCHECK_RHN_USER"
      RHNCHECK_RHN_PASSWORD = "$RHNCHECK_RHN_PASSWORD"
      RHNCHECK_SYSTEMS = "$RHNCHECK_SYSTEMS"
      RHSM_OFFLINE_TOKEN = "$RHSM_OFFLINE_TOKEN"
      RHN_ACTIVATION_KEY = "$RHN_ACTIVATION_KEY"
      RHN_ORG = "$RHN_ORG"
      TAG = "${PREFIX}_reposync"
    }

    resources {
      cpu = 3000 # Mhz
      memory = 5120 # MB
    }

  }
}
