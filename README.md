# Reposync job

To add new repos, add the repo file to [prod.repos.d](prod.repos.d/), the
GPG key to [gpgkeys](gpgkeys/) and any specific configuration to [prod.repos.yaml](prod.repos.yaml).
This last part is probably not needed.

If you're adding a redhat repo, check existing redhat repos and copy the same logic (which is defining a [custom_script](https://gitlab.cern.ch/linuxsupport/cronjobs/reposync/-/blob/master/reposync/rhn_update_cert_symlink.sh))

## linuxsoft.cern.ch paths

By default all repos will be mirrored under <https://linuxsoft.cern.ch/mirror/>

You can control the path with `prod.repos.yaml` file, by using `pathroot` as in:

```yaml
redhat-8-ev-x86_64.repo:
  pathroot: ''
```

This will make mirrors start on <https://linuxsoft.cern.ch/> instead.

PS: Be aware RH repos are blocked unless you belong to certain LANDB sets: <https://linuxops.web.cern.ch/support/redhat/#landb-sets>

# Downloading Redhat certificates

Certs for linuxsoft-mirror system are registered via [RHN](https://access.redhat.com/management/systems/b4ec8c2d-3eae-4ae0-b8fa-ec6d8a08ce9f/subscriptions)
Manual downloading of certs is a thing of the past as this is now performed automagically via the [rhncheck.py script](https://gitlab.cern.ch/linuxsupport/cronjobs/reposync/-/blob/master/reposync/rhncheck.py)

Whilst no longer required, you can use the following command to determine what certificate maps to which entitlement:

```
[root@lxsoftadm28 ~]# for i in /etc/cdn.redhat.com/*pem; do echo -n "$i: "; subscription-manager import --certificate $i >/dev/null; subscription-manager list --consumed |grep "Subscription Name" | cut -d: -f2; subscription-manager remove --all >/dev/null; done
/etc/cdn.redhat.com/5378820407501503083.pem:    Red Hat Enterprise Linux for Real Time, Premium (Physical Node)
/etc/cdn.redhat.com/1971273915703326979.pem:    Red Hat Enterprise Linux Developer Suite
/etc/cdn.redhat.com/3184188042360253346.pem:    Red Hat Virtualization (2-sockets), Premium
/etc/cdn.redhat.com/2574189359922386789.pem:    Red Hat Enterprise Linux Extended Life Cycle Support (Physical or Virtual Nodes)
/etc/cdn.redhat.com/4333396096500643205.pem:    Red Hat Enterprise Linux Developer Suite
```

# RedHat repos

Figuring out which RedHat repos to sync is not obvious as paths change between versions (i.e. RHEL7 use different repo URLs than RHEL8).

You could always spawn a new RHELX machine and follow these steps:

* Share the RH image with the tenant you want
```
eval $(ai-rc 'IT Linux Support - CI VMs')
openstack image list | grep RHEL  ## To see all available images
# replace with the uuid of destination project
openstack image add project '$uuid-of-image' '$uuid-of-project'
```

* Spawn a machine with that image, select your private key when creating it
* Quickly add this machine to `LINUXSOFT RHEL LICENSED GPN` so it has access to RH repos for installation
* ssh as `cloud-user`: `ssh cloud-user@yournode`, then `sudo -i`
* Edit `/root/.ssh/authorized_keys` and remove everything before your ssh key
* Allow access to the rest of the team. Install the latest cern-linuxsupport-access and enable it:
  ```
  $ yum install http://linuxsoft.cern.ch/cern/centos/8/CERN/x86_64/Packages/cern-linuxsupport-access-1.2-1.el8.cern.noarch.rpm
  $ cern-linuxsupport-access enable
  ```
* `subscription-manager register --username yourrhaccount@cern.ch`. It will ask for your RH access password
* `subscription-manager repos --list` will list all the repos and their URLs. You can now add those that you need.

## Sample RH nodes

* As of 4/12/2020 these nodes are available for our team:
  * `lx-rh7-certs` for RHEL 7
  * `rhel8-sample` for RHEL 8
