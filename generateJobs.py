#!/usr/bin/python3

import sys
import os
import getopt
import yaml
from string import Template
import configparser
from configparser import DuplicateSectionError
import base64


try:
  opts, args = getopt.getopt(sys.argv[1:], "c:t:y:", ["config=", "template=", "yumdir="])
except getopt.GetoptError:
  print('generateJobs.py -c <configfile> -t <templatefile> -y <yumdir>')
  sys.exit(1)

config_name = None
template_name = 'reposync.nomad.tpl'
for opt, arg in opts:
  if opt in ("-c", "--config"):
    config_name = arg
  elif opt in ("-t", "--template"):
    template_name = arg
  elif opt in ("-y", "--yumdir"):
    yumdir_name = arg

if not config_name:
  print('Missing config file')
  sys.exit(1)

if not yumdir_name:
  print('Missing yum.repos.d/ directory')
  sys.exit(1)

with open(config_name, 'r') as configfile:
  config = yaml.safe_load(configfile)

with open(template_name, 'r') as templatefile:
  template = Template(templatefile.read())

try:
  def_schedule         = config['defaults']['schedule']
  def_checksum         = config['defaults']['checksum']
  def_pathroot         = config['defaults']['pathroot']
  def_pathcut          = config['defaults']['pathcut']
  def_reposync         = config['defaults']['run_reposync']
  def_runcustomscript  = config['defaults']['run_customscript']
  def_customscript     = config['defaults']['customscript']
  def_skippackagecount = config['defaults']['skip_package_count']
except IndexError:
  print('Missing configuration options')
  sys.exit(2)


for repofile in os.listdir(yumdir_name):

  try:
    repoconfig = configparser.ConfigParser(interpolation=None)
    print('Reading {0}'.format(repofile))
    with open(os.path.join(yumdir_name, repofile), 'r') as f:
      repoconfig.read_file(f)
  except IOError as e:
    print('error while reading repo {0}: {1}'.format(os.path.join(yumdir_name, repofile), str(e)))
    sys.exit(3)
  except DuplicateSectionError as e:
    print('duplicate declaration in repo {0}: {1}'.format(os.path.join(yumdir_name, repofile), str(e)))
    sys.exit(3)

  for rid in repoconfig.sections():
    if not repoconfig[rid].getboolean('enabled', True):
      continue
    try:
      SCHEDULE = config[repofile]['schedule']
    except KeyError:
      SCHEDULE = def_schedule
    try:
      CHECKSUM = config[repofile]['checksum']
    except KeyError:
      CHECKSUM = def_checksum
    try:
      PATHROOT = config[repofile]['pathroot']
    except KeyError:
      PATHROOT = def_pathroot
    try:
      PATHCUT = config[repofile]['pathcut']
    except KeyError:
      PATHCUT = def_pathcut
    try:
      RUN_REPOSYNC = config[repofile]['run_reposync']
    except KeyError:
      RUN_REPOSYNC = def_reposync
    try:
      RUN_CUSTOMSCRIPT = config[repofile]['run_customscript']
    except KeyError:
      RUN_CUSTOMSCRIPT = def_runcustomscript
    try:
      CUSTOMSCRIPT = config[repofile]['customscript']
    except KeyError:
      CUSTOMSCRIPT = def_customscript
    try:
      SKIP_PACKAGE_COUNT = config[repofile]['skip_package_count']
    except KeyError:
      SKIP_PACKAGE_COUNT = def_skippackagecount



    url = repoconfig[rid]['baseurl']
    if url == 'None':
        url = ''

    path = url.split(PATHCUT)[-1].lstrip('/')
    if PATHROOT:
      path = os.path.join(PATHROOT.strip('/'), path)

    # Recreate the repo file
    yumfile = base64.b64encode('\n'.join(['='.join(x) for x in repoconfig.items(rid)]).encode()).decode()

    data = {
      'REPOID'             : rid,
      'REPOPATH'           : path,
      'REPOFILE'           : yumfile,
      'CHECKSUM'           : CHECKSUM,
      'SCHEDULE'           : SCHEDULE,
      'CUSTOMSCRIPT'       : CUSTOMSCRIPT,
      'RUN_REPOSYNC'       : 1 if RUN_REPOSYNC else 0,
      'RUN_CUSTOMSCRIPT'   : 1 if RUN_CUSTOMSCRIPT else 0,
      'SKIP_PACKAGE_COUNT' : 1 if SKIP_PACKAGE_COUNT else 0,
    }

    jobfile = '{0}_reposync_{1}.nomad'.format(os.getenv('PREFIX', 'dev'), rid)
    with open(jobfile, 'w') as job:
      job.write(template.safe_substitute(data))
      print('Generated job: {0}'.format(jobfile))

