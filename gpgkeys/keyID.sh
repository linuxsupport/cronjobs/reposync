#! /bin/bash

KEY_PATH=$1
KEY_FINGERPRINT=$(gpg --show-keys ${KEY_PATH} | sed -n 2p | tr -d ' ' | tr '[:upper:]' '[:lower:]')

echo "RPM key ID may be one of:"
echo ${KEY_FINGERPRINT} | grep -o '.\{8\}$'
echo ${KEY_FINGERPRINT} | grep -o '.\{16\}$'

